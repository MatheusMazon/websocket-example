import React, { useEffect, useState, useMemo } from 'react';
import DataBox from './page'
import './App.css';



function App() {

  const [data, setData] = useState([])
  const [camera, setCamera] = useState(null)

  const socket = useMemo(() => new WebSocket('ws://localhost:8000/data'), [])


  useEffect(() => {

    socket.onmessage = function (event) {
      setData(JSON.parse(event.data))

      //setCamera()
    }
  }, [data, socket])



  return (
    <div className="App">
      <canvas width={400} height={200} />
      <img src={camera} alt="camera" className="camera-img" />
      <DataBox data={data.data1}></DataBox>
      <DataBox data={data.data2}></DataBox>
      <DataBox data={data.data3}></DataBox>
      <DataBox data={data.data4}></DataBox>
    </div>


  );
}

export default App;
