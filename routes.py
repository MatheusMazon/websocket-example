from sanic.response import json, file
from sanic import Blueprint
import asyncio
import numpy as np
from websockets.exceptions import ConnectionClosed
import json

bp = Blueprint('my_blueprint')

bp.static('/', './frontend/build')


@bp.route('/')
async def bp_root(request):
    return await file('frontend/build/index.html')


connections = set()


@bp.websocket('/data')
async def bp_data(request, ws):
    '''
    while True:
        connections.add(ws)

        print(len(connections))

        for connection in connections.copy():

            try:
                await connection.send(str(np.random.random_sample()) + '-data1')
            except ConnectionClosed:
                connections.remove(connection)
        await asyncio.sleep(2)
    '''

    # print(ws)
    while True:
        data = {}
        data['data1'] = str(np.random.random_sample())[:6]
        data['data2'] = str(np.random.random_sample())[:6]
        data['data3'] = str(np.random.random_sample())[:6]
        data['data4'] = str(np.random.random_sample())[:6]

        data = json.dumps(data)
        print('Sending: ' + data)

        await ws.send(data)
        await asyncio.sleep(2)
